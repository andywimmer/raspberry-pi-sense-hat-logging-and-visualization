# Raspberry Pi - Sense HAT Logging and Visualization

Logging Raspberry Pi [Sense HAT](https://www.raspberrypi.org/products/sense-hat/) readings to a time series database for data visualization purposes.

**Contents**

<!-- TOC -->

- [Overview](#overview)
- [Materials](#materials)
- [Raspbian Setup & Config](#raspbian-setup--config)
- [InfluxDB](#influxdb)
- [Telegraf](#telegraf)
- [Sense HAT](#sense-hat)
- [Chronograf](#chronograf)
  - [Visualize Sense HAT Data](#visualize-sense-hat-data)
- [Footnotes](#footnotes)

<!-- /TOC -->

## Overview

The practical objective is to monitor ambient temperature over an extended period of time and visualize the data as a chart. Humidity and pressure are also monitored, thus construction of a basic 'weather station' dashboard is ultimately possible.

![](img/example-dashboard.png)

This procedure consists of installing [Raspbian](https://www.raspberrypi.org/documentation/raspbian/) (Buster), basic system configuration, installation of prerequisite software, configuration thereof, and steps to visualize the collected data.

> :pushpin: Steps for a new installation of Raspbian (Buster) are included for sake of completeness. In most scenarios an existing and updated installation may be used.

Aside from the [Sense HAT libraries](https://github.com/astro-pi/python-sense-hat), three components of the [TICK Stack](https://www.influxdata.com/time-series-platform/) are utilized: [Telegraf](https://docs.influxdata.com/telegraf/v1.13/), [InfluxDB](https://docs.influxdata.com/influxdb/v1.7/), and [Chronograf](https://docs.influxdata.com/chronograf/v1.7/) respectively.

## Materials

Required:
- Raspberry Pi Sense HAT
- Supported Raspberry Pi (3 B+ is used in this example)
- SD card

Optional:
- 6-12" GPIO ribbon cable
    > :bulb: Physically distancing the Sense HAT from any ambient heat sources - such as the Raspberry Pi itself - may be considered to achieve maximum sensor accuracy.

## Raspbian Setup & Config

The 'control' system used here may vary - in this scenario macOS is used. In general, the SD card is flashed with the latest version of [Raspbian Buster Lite](https://www.raspberrypi.org/downloads/raspbian/), configured for remote access and WiFi on the control system, and then the SD card is moved to the Raspberry Pi for initial boot and further configuration remotely via SSH - the typical steps for a headless setup.

1. Flash SD card using preferred utility (E.g. Etcher)
1. Enable SSH by creating a blank file named `ssh` in the root directory of the SD card
    - E.g. via macOS:
        ``` sh
        touch /Volumes/boot/ssh
        ```
1. Enable WiFi by creating the file `wpa_supplicant.conf` in the root directory of SD card - adjusting `ssid` and `psk` values appropriately:
    - E.g. via macOS, the file would be created at `/Volumes/boot/wpa_supplicant.conf`

> Example `wpa_supplicant.conf`
>
>   ``` conf
> country=US
> ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
> update_config=1
> network={
>     ssid="YourNetworkSSID"
>     psk="YourNetworkPassphrase"
>     scan_ssid=1
>     key_mgmt=WPA-PSK
> }
>   ```
4. Insert the SD card into Raspberry Pi and boot
1. Determine the IP address of Raspberry Pi [^1] and connect via SSH:
    ``` sh
    ssh pi@192.168.1.55
    ```
    - > :bulb: The default password is `raspberry`
1. Once connected, enter the interactive configuration utility:
    ``` sh
    sudo raspi-config
    ```
    1. Change the default **Password**
    1. Ensure **Timezone** is set correctly
    1. Exit `raspi-config` and reboot if prompted
1. Update the system:
    ``` sh
    sudo apt update && sudo apt upgrade -y
    ```
    - > :warning: This may be a time-consuming step depending on the number of updates available and/or network conditions.
1. Reboot again for sanity:
    ``` sh
    sudo reboot
    ```

## InfluxDB

First we'll install the database.

1. Download and add the InfluxData repository key:
    ``` sh
    wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
    ```
1. Add the InfluxData repository to the `apt` source list:
    ``` sh
    echo "deb https://repos.influxdata.com/debian buster stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
    ```
1. Update `apt` and install InfluxDB:
    ``` sh
    sudo apt update && sudo apt install influxdb -y
    ```
1. Enable the service:
    ``` sh
    sudo systemctl unmask influxdb && sudo systemctl enable influxdb
    ```
1. Start the service:
    ``` sh
    sudo systemctl start influxdb
    ```
1. Use the `influx` command to perform the initial connection to InfluxDB:
    ``` sh
    influx
    ```
1. Once connected, create an admin user with the following (setting the PASSWORD as desired):
    ``` sh
    CREATE USER admin WITH PASSWORD 'myweakpasswd' WITH ALL PRIVILEGES
    ```
1. Confirm the user was created using the `SHOW USERS` command:
    ``` sh
    SHOW USERS
    ```
    - Expected output:
        ``` sh
        user  admin
        ----  -----
        admin true
        ```
1. Use the `EXIT` command to return to bash:
    ``` sh
    EXIT
    ```
1. Edit the file `/etc/influxdb/influxdb.conf` to uncomment / update the following items in the `[http]` key:
> Excerpt from `/etc/influxdb/influxdb.conf`
>    ``` conf
>    [http]
>    # Determines whether HTTP endpoint is enabled.
>    enabled = true
>
>    # The bind address used by the HTTP service.
>    bind-address = ":8086"
>
>    # Determines whether user authentication is enabled over HTTP/HTTPS.
>    auth-enabled = true
>
>    # Determines whether detailed write logging is enabled.
>    write-tracing = false
>    ```
11. Restart the service:
    ``` sh
    sudo systemctl restart influxdb
    ```

## Telegraf

With the database ready we move on to the collector service.

1. Install Telegraf:
    ``` sh
    sudo apt install telegraf -y
    ```
1. Edit the file `/etc/telegraf/telegraf.conf` to uncomment / update the `[[outputs.influxdb]]` key, using the `username` and `password` set previously:
> Excerpt from `/etc/telegraf/telegraf.conf`
>    ``` conf
>    [[outputs.influxdb]]
>        ## The full HTTP or UDP URL for your InfluxDB instance.
>        ##
>        ## Multiple URLs can be specified for a single cluster, only ONE of the
>        ## urls will be written to each interval.
>        # urls = ["unix:///var/run/influxdb.sock"]
>        # urls = ["udp://127.0.0.1:8089"]
>        urls = ["http://127.0.0.1:8086"]
>
>        ## The target database for metrics; will be created as needed.
>        database = "telegraf"
>
>        ## Timeout for HTTP messages.
>        timeout = "5s"
>
>        ## HTTP Basic Auth
>         username = "admin"
>         password = "myweakpasswd"
>    ```
3. Start and restart the service:
    ``` sh
    sudo systemctl start telegraf && sudo systemctl restart telegraf
    ```
    - > :warning: Occasionally the telegraf service will throw errors if this isn't done.
1. Connect to InfluxDB as the admin user with the previously set password:
    ``` sh
    influx -username admin -password myweakpasswd
    ```
1. Check databases using the `SHOW DATABASES` command:
    ``` sh
    SHOW DATABASES
    ```
    - Expected output:
        ``` sh
        name: databases
        name
        ----
        _internal
        telegraf
        ```
        - > :bulb: Both databases are generated automatically by the telegraf service, as it collects basic system data by default. Their presence illustrate that the service is collecting data as intended.
1. Use the `EXIT` command to return to bash:
    ``` sh
    EXIT
    ```

## Sense HAT

The collector service is feeding data into the database, so the next step is to have it collect our desired Sense HAT data as well. We'll use a python script and Telegraf's built-in [`exec` plugin](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/exec) to achieve this.

1. First install the Sense HAT libraries:
    ``` sh
    sudo apt install sense-hat -y
    ```
1. The python script will be executed by the `telegraf` user, so ensure it has proper permissions by adding it to a couple of groups:
    ``` sh
    sudo usermod -aG input telegraf && sudo usermod -aG i2c telegraf
    ```
1. Update permissions on the `/etc/telegraf` directory:
    ``` sh
    sudo chown -R telegraf:telegraf /etc/telegraf/
    ```
1. Create a python script and make it executable:
    ``` sh
    sudo touch /bin/sense.py && sudo chmod +x /bin/sense.py
    ```
1. Use your preferred editor to insert this script, which outputs temperature, humidity, and pressure in JSON format:
> Example `/bin/sense.py`:
>    ``` python
>    #!/usr/bin/python
>    from sense_hat import SenseHat
>
>    sense = SenseHat()
>    temp_c = sense.get_temperature()
>    temp_f = 1.8 * temp_c + 32
>    humidity = sense.get_humidity()
>    pressure = sense.get_pressure()
>
>    print('{"temp_c":% 5f, "temp_f":% 5f, "humidity":% 5f, "pressure":% 5f}' %(temp_c, temp_f, humidity, pressure))
>    ```
6. Test script execution by the intended `telegraf` user:
    ``` sh
    sudo -u telegraf /bin/sense.py
    ```
    - Expected output (values will vary):
        ``` sh
        {"temp_c": 17.978970, "temp_f": 64.362145, "humidity": 60.755447, "pressure": 1004.260254}
        ```
1. Edit the file `/etc/telegraf/telegraf.conf` to uncomment / update the `[[inputs.exec]]` key:
> Excerpt from `/etc/telegraf/telegraf.conf`
>    ``` conf
>    [[inputs.exec]]
>      ## Commands array
>      commands = ["/bin/sense.py"]
>
>      ## Timeout for each command to complete.
>      timeout = "5s"
>
>      ## measurement name suffix
>      name_suffix = "_sense"
>
>      ## Data format to consume.
>      data_format = "json"
>    ```
7. Restart telegraf:
    ``` sh 
    sudo systemctl restart telegraf
    ```

Telegraf is now running the `exec` plugin to execute the python script, feeding (or "inputting") the Sense HAT data JSON string into the database.

## Chronograf

Finally, we want to see the data with a visualization tool.

1. Install Chronograf:
    ``` sh
    sudo apt install chronograf -y
    ```
1. Navigate to `http://<raspberry.pi.ip.address>:8888` via browser on the control system
1. Complete the form to create the InfluxDB connection
    - Skip the Kapacitor step
1. Selecting the eyeball menu item (**Host List**) will display a list of servers
1. Selecting your server from the list displays a basic dashboard of the system data collected by default via the telegraf service

### Visualize Sense HAT Data

Data from the Sense HAT can be visualized using the data explorer and/or by creating a simple dashboard.

**Data Explorer**

![](img/explore.png)

1. Selecting this icon (beneath the eyeball) allows you to query InfluxDB directly
1. Select `telegraf.autogen` from the _DB.RetentionPolicy_ list in the lower left quadrant of the Data Explorer
1. Select `exec_sense` from the _Measurements & Tags_ list
1. Select an item from the _Fields_ list to display a basic chart
1. An InfluxQL query will be displayed below the chart, e.g.:
    ```
    SELECT mean("temp_f") AS "mean_temp_f" FROM "telegraf"."autogen"."exec_sense" WHERE time > :dashboardTime: GROUP BY time(:interval:) FILL(null)
    ```
    - > :bulb: The query displayed may be copied from this screen for later use to quickly create a dashboard element

**Dashboards**

![](img/dashboards.png)

The third menu item (**Dashboards**) should be self-explanitory. Selecting `Create Dashboard` provides a basic starting point. Use the above **Data Explorer** steps 2-4 to build the desired dashboard elements.

> :exclamation: You can also import the [Sense-HAT.json](Sense-HAT.json) file of the example dashboard used here as a basic starting template:
> 
> ![](img/example-dashboard.png)

## Footnotes

[^1]: Methods vary here. If you're keen to the address space on your network, running `arp -a` from the control system may be enough to determine the assigned IP address. Other network scanning utilities may be utilized if necessary.
